/*
 * bit_reader.h
 *
 * author: smkim, buttonfly@gmail.com *
 * release date: 2008.4.14
 */

#ifndef GURUM_BASE_BIT_READER_H_
#define GURUM_BASE_BIT_READER_H_

#include <cstdint>
#include <cstddef>
#include "common.h"

namespace base {

class BitReader {
public:
  BitReader(const uint8_t *buf, size_t len);
  virtual ~BitReader();

  void ReadN(void* dst, size_t len);
  void Skip(size_t len);
  uint8_t Read1(int s=0, int n=8);
  uint16_t  Read2(int s=0, int n=16);
  uint32_t Read4(int s=0, int n=32);
  uint8_t Peek1(int s=0, int n=8);
  uint16_t Peek2(int s=0, int n=16);
  uint32_t Peek4(int s=0, int n=32);

  size_t CurrentPos();
  void Seek(size_t pos);

private:
  const uint8_t *ptr_;
  size_t pos_=0;
  const size_t len_;
};

} /* namespace base */

#endif /* GURUM_BASE_BIT_READER_H_ */
