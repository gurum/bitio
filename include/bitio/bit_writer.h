/*
 * bit_writer.h
 *
 * author: smkim, buttonfly@gmail.com *
 * release date: 2008.4.14
 */

#ifndef GURUM_BASE_BIT_WRITER_H_
#define GURUM_BASE_BIT_WRITER_H_

#include <cstdint>
#include <cstddef>
#include "common.h"

namespace base {

class BitWriter {
public:
  BitWriter(size_t len);
  virtual ~BitWriter();

  /**
   * write n bytes data to this ioutil's buffer
   * @param ioutil this ioutil
   * @param size the size to write
   * @param src the data to be store in the ioutil's buffer
   */
  void WriteN(void* src, size_t len);
  /**
   * write 4 bytes data to this ioutil's buffer
   * @param ioutil this ioutil
   * @param src the 4 bytes data to be stored
   */
  void Write4(uint32_t src);
  /**
   * write 2 bytes data to this ioutil's buffer
   * @param ioutil this ioutil
   * @param src the 2 bytes data to be stored
   */
  void Write2(uint16_t src);
  /**
   * write 1 byte data to this ioutil's buffer
   * @param ioutil this ioutil
   * @param src the 1 byte data to be stored
   */
  void Write1(uint8_t src);

  const uint8_t *Ptr();

private:
  uint8_t *ptr_=nullptr;
  size_t len_=0;
  size_t pos_=0;
};

} /* namespace base */

#endif /* GURUM_BASE_BIT_WRITER_H_ */
