/*
 * common.h
 *
 * author: smkim, buttonfly@gmail.com *
 * release date: 2008.4.14
 */

#ifndef GURUM_BASE_BIT_COMMON_H_
#define GURUM_BASE_BIT_COMMON_H_

#include <cstdint>
#include <stdio.h>

#define H_(buffer, buffer_len)   do{  \
                    int _kn_;   \
                    fflush(stderr); \
                    for(_kn_ = 0 ; _kn_ < buffer_len; _kn_++){  \
                      if((_kn_ != 0) && ((_kn_ %0x10) == 0)){ \
                        fprintf(stderr, "\n");  \
                      } \
                      fprintf(stderr, "%02x ", buffer[_kn_]); \
                    } \
                    fprintf(stderr, "\n");  \
                  }while(0)

namespace base {

static
uint8_t GetBits8(const uint8_t* x, int s, int n) {
  return ((unsigned char) (((x)[0] >> (s)) & ~(~0 << (n))));
}

static
uint16_t GetBits16(const uint8_t* x, int s, int n) {
  return ((unsigned short) (((((unsigned short)((x)[0]) << 8) | (unsigned short)((x)[1])) >> (s)) & ~(~0 << (n))));
}

static
uint32_t GetBits32(const uint8_t* x, int s, int n) {
  return (unsigned int) (((((unsigned int)((x)[0]) << 24) | \
            ((unsigned int)((x)[1]) << 16) | \
            ((unsigned int)((x)[2]) << 8)| \
            (unsigned int)((x)[3])) >> (s)) /*& ~(0xFFFFFFFF << (n))*/);
}

}

#endif /* GURUM_BASE_BIT_COMMON_H_ */
