/*
 * bit_reader.cc
 *
 * author: smkim, buttonfly@gmail.com *
 * release date: 2008.4.14
 */

#include "bit_reader.h"
#include <string.h>

namespace base {

BitReader::BitReader(const uint8_t *buf, size_t len)
: ptr_(buf), len_(len) {
}

BitReader::~BitReader() {
}

void BitReader::ReadN(void* dst, size_t len) {
  if(dst) memcpy(dst, &ptr_[pos_], len);
  pos_ += len;
}

void BitReader::Skip(size_t len) {
  if(len > 0) ReadN(nullptr, len);
}

uint8_t BitReader::Read1(int s, int n) {
  int8_t tmp = Peek1(s, n);
  ReadN(nullptr, 1);
  return tmp;
}

uint16_t BitReader::Read2(int s, int n) {
  int16_t tmp = Peek2(s, n);
  ReadN(nullptr, 2);
  return tmp;
}

uint32_t BitReader::Read4(int s, int n) {
  int32_t tmp = Peek4(s, n);
  ReadN(nullptr, 4);
  return tmp;
}

uint8_t BitReader::Peek1(int s, int n) {
  return GetBits8(&ptr_[pos_], s, n);
}

uint16_t BitReader::Peek2(int s, int n) {
  return GetBits16(&ptr_[pos_], s, n);
}

uint32_t BitReader::Peek4(int s, int n) {
  return GetBits32(&ptr_[pos_], s, n);
}

size_t BitReader::CurrentPos() {
  return pos_;
}

void BitReader::Seek(size_t pos) {
  pos_= pos;
}

} /* namespace base */
