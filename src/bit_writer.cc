/*
 * bit_writer.cc
 *
 * author: smkim, buttonfly@gmail.com *
 * release date: 2008.4.14
 */

#include "bit_writer.h"
#include <string.h>
#include <stdlib.h>

using namespace std;

namespace base {

BitWriter::BitWriter(size_t len)
: len_(len) {
  ptr_ = (uint8_t *) malloc(len);
}

BitWriter::~BitWriter() {
  if(ptr_)
    free(ptr_);
  ptr_ = nullptr;
}

/**
 * write n bytes data to this ioutil's buffer
 * @param ioutil this ioutil
 * @param size the size to write
 * @param src the data to be store in the ioutil's buffer
 */
void BitWriter::WriteN(void* src, size_t len) {
  memcpy((void *) &ptr_[pos_], src, len);
  pos_ += len;
}

/**
 * write 4 bytes data to this ioutil's buffer
 * @param ioutil this ioutil
 * @param src the 4 bytes data to be stored
 */
void BitWriter::Write4(uint32_t src) {
  uint8_t *tmp = (uint8_t *) &src;
  src =  GetBits32(tmp, 0, 8 * 4);
  WriteN(&src, 4);
}

/**
 * write 2 bytes data to this ioutil's buffer
 * @param ioutil this ioutil
 * @param src the 2 bytes data to be stored
 */
void BitWriter::Write2(uint16_t src) {
  uint8_t *tmp = (uint8_t *) &src;
  src =  GetBits16(tmp, 0, 8 * 2);
  WriteN(&src, 2);
}

/**
 * write 1 byte data to this ioutil's buffer
 * @param ioutil this ioutil
 * @param src the 1 byte data to be stored
 */
void BitWriter::Write1(uint8_t src) {
  uint8_t *tmp = (uint8_t *) &src;
  src =  GetBits8(tmp, 0, 8 * 1);
  WriteN(&src, 1);
}

const uint8_t *BitWriter::Ptr() {
  return ptr_;
}

} /* namespace base */
