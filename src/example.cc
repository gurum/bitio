/*
 ============================================================================
 Name        : exampleProgram.c
 Author      : buttonfly
 Version     :
 Copyright   : Your copyright notice
 Description : Uses shared library to print greeting
               To run the resulting executable the LD_LIBRARY_PATH must be
               set to ${project_loc}/libsimplenet/.libs
               Alternatively, libtool creates a wrapper shell script in the
               build directory of this program which can be used to run it.
               Here the script will be called exampleProgram.
 ============================================================================
 */

#include <stdio.h>
#include "bitio/bit_reader.h"
#include "bitio/bit_writer.h"
#include <cstdint>
#include <unistd.h>
#include  <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <iostream>
#include <string.h>
#include <string>

static int FileSize(int fd) {
  int ret;
  struct stat buf;
  memset(&buf, 0, sizeof(buf));
  ret = fstat(fd, &buf);
  if(ret < 0) return -1;

  return buf.st_size;
}

static uint8_t* ReadFile(const char* path, int* _size) {
  int fd;
  int size;
  uint8_t* buffer;
  int ret;
  fd = open(path, O_RDONLY ,S_IRUSR|S_IWUSR);
  if(fd == -1)
  {
       return NULL;
  }
  size = FileSize(fd);
  buffer = (uint8_t*) malloc(size + 1);
  memset(buffer, '\0', size + 1);
  ret = read(fd, buffer, size);
  if(ret != size)
  {
  }
  if(_size != NULL)
  {
      *_size = size;
  }
  close(fd);
  return buffer;
}

int TestOpus(const char *file) {
  int len;
  uint8_t *buf = ReadFile(file, &len);
  assert(buf);

//  H_(buf, len);

  base::BitReader reader(buf, len);
  reader.ReadN(nullptr, 28); // ogg header

//  {
    // OpusHead
    char tmp[64] = {0,};
    reader.ReadN(tmp, 8);
    std::cerr << tmp << std::endl;
//  }

//  {
    // version
    int version = reader.Read1();
    std::cerr << "version: " << version << std::endl;
//  }

//  {
    // channel count
    int channel_count = reader.Read1();
    std::cerr << "channel count:" << channel_count << std::endl;
//  }

//  {
    // pre skip
    int pre_skip = reader.Read2();
    std::cerr << "pre-skip:" << pre_skip << std::endl;
//  }

//  {
    // sample rate
    int samplerate = reader.Read4();
    std::cerr << "samplerate:" << samplerate << std::endl;
//  }

//  {
    // output gain
    int output_gain = reader.Read2();
    std::cerr << "output_gain:" << output_gain << std::endl;
//  }

//  {
    // channel map
    int channel_map = reader.Read1();
    std::cerr << "channel_map:" << channel_map << std::endl;
//  }

    uint8_t tmp2[64];
    base::BitWriter writer(64);
    writer.WriteN(tmp, 8);
    writer.Write1((uint8_t)version);
    writer.Write1((uint8_t)channel_count);
    writer.Write2((uint16_t)pre_skip);
    writer.Write4((uint32_t)samplerate);
    writer.Write2((uint16_t)output_gain);
    writer.Write1((uint8_t)channel_map);

//    writer.Ptr();
    H_(writer.Ptr(), 32);
}

int main(int argc, char *argv[]) {

  if(argc < 2) {
    fprintf(stderr, "%s file-path", argv[0]);
    return 0;
  }

  TestOpus(argv[1]);

  return 0;
}
